## *Blastobotrys adeninivorans* LS3

### Source

* **Data source**: [ENA](https://www.ebi.ac.uk/ena/browser/home)
* **BioProject**: [PRJEB4557](https://www.ebi.ac.uk/ena/browser/view/PRJEB4557)
* **Assembly accession**: No assembly accession available, ENA records from HG937690 to HG937694
* **Original submitter**: 

### Assembly overview

* **Assembly level**: Chromosome
* **Assembly name**: 
* **Assembly length**: 11,836,278
* **#Chromosomes**: 5
* **Mitochondiral**: Yes
* **N50 (L50)**: 3,827,910 (2)

### Annotation overview

* **Original annotator**: 
* **CDS count**: 6136
* **Pseudogene count**: 34
* **tRNA count**: 170
* **rRNA count**: 51
* **Mobile element count**: 1
